val scala3Version = "3.2.1"

ThisBuild / scalafixDependencies += "com.github.liancheng" %% "organize-imports" % "0.6.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "uncaged",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,
    semanticdbEnabled := true,                     // For scalafix
    scalacOptions ++= Seq(
      "-deprecation",                              // Emit warning and location for usages of deprecated APIs.
      "-feature",                                  // Emit warning and location for usages of features that should be imported explicitly.
      "-unchecked",                                // Enable additional warnings where generated code depends on assumptions.
      "-Xfatal-warnings",                          // Fail the compilation if there are any warnings.
    ),

    libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "2.1.0",
    libraryDependencies += "org.typelevel" %% "cats-core" % "2.9.0",
    libraryDependencies += "org.typelevel" %% "alleycats-core" % "2.9.0",
    libraryDependencies += "io.circe" %% "circe-core" % "0.14.1",
    libraryDependencies += "io.circe" %% "circe-generic" % "0.14.1",
    libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.14",
    libraryDependencies += "com.lihaoyi" %% "pprint" % "0.7.0",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.14" % "test"
  )
