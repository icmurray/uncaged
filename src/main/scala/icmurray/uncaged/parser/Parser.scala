package icmurray.uncaged.parser

import scala.util.parsing.combinator._

trait Parser {
  def parse(script: String): Either[String, ProgramDefn]
}

object Parser {

  def apply(): Parser = new Parser {
    override def parse(script: String): Either[String, ProgramDefn] =
      Impl.parse(Impl.program, script) match {
        case Impl.Success(matched, _) => Right(matched)
        case Impl.Failure(msg, _)     => Left(msg)
        case Impl.Error(msg, _)       => Left(msg)
      }
  }

  private object Impl extends RegexParsers {

    def program = phrase(programDefn)

    def programDefn: Parser[ProgramDefn] =
      ioFuncDefn ~ rep(functionDefn) ^^ { case main ~ functions => ProgramDefn(main, functions) }

    def functionDefn: Parser[FuncDefn] =
      ioFuncDefn | pureFuncDefn

    def ioFuncDefn: Parser[IOFuncDefn] =
      "def" ~ name ~ args ~ "=" ~ "do" ~ statements ~ returns <~ ";" ^^ {
        case _ ~ name ~ args ~ _ ~ _ ~ statements ~ returns =>
          IOFuncDefn(name, args, statements, returns)
      }

    def pureFuncDefn: Parser[PureFuncDefn] =
      pureLetFuncDefn | pureExprFuncDefn

    def pureLetFuncDefn: Parser[PureFuncDefn] =
      "def" ~ name ~ args ~ "=" ~ rep(letStatement) ~ inExpression <~ ";" ^^ {
        case _ ~ name ~ args ~ _ ~ statements ~ returns =>
          PureFuncDefn(name, args, statements, returns)
      }

    def pureExprFuncDefn: Parser[PureFuncDefn] =
      "def" ~> name ~ args ~ "=" ~ extendedPureExpression <~ ";" ^^ { case name ~ args ~ _ ~ returns =>
        PureFuncDefn(name, args, Nil, returns)
      }

    def name: Parser[String] =
      """[a-zA-Z_]([a-zA-Z0-9_])*""".r ^^ { _.toString }

    def binder: Parser[Binder] =
      name.map(Binder.apply)

    def args: Parser[List[Binder]] =
      rep(binder)

    def statement: Parser[Statement] =
      letStatement | doStatement

    def letStatement: Parser[Statement] =
      "let" ~ binder ~ "=" ~ extendedPureExpression ^^ { case _ ~ binder ~ _ ~ expr =>
        Statement(binder, Lift(expr))
      }

    def doStatement: Parser[Statement] =
      binder ~ "<-" ~ ioExpression ^^ { case binder ~ _ ~ expr =>
        Statement(binder, expr)
      }

    def inExpression: Parser[PureExpression] =
      "in" ~ pureExpression ^^ { case _ ~ expr => expr }

    def statements: Parser[List[Statement]] = rep(statement)

    def ioExpression: Parser[IOExpression] = http

    def http: Parser[HttpCall] =
      "http.get" ~ "(" ~ url ~ "," ~ httpQuery ~ ")" ^^ { case _ ~ _ ~ url ~ _ ~ query ~ _ =>
        HttpCall("GET", url, query)
      } |
        "http.get" ~ "(" ~ url ~ ")" ^^ { case _ ~ _ ~ url ~ _ =>
          HttpCall("GET", url, RecordLiteral(Map.empty))
        }

    def url: Parser[PureExpression] = pureExpression

    def httpQuery: Parser[PureExpression] = pureExpression

    def extendedPureExpression: Parser[PureExpression] = ifExpression
      | pureExpression

    def pureExpression: Parser[PureExpression] = binOp
      | pureTerm

    def pureTerm: Parser[PureExpression] = pureApplication
      | dotAccessor
      | literal
      | variable

    def literal: Parser[Literal] = stringLiteral
      | numberLiteral
      | longLiteral
      | recordLiteral

    def stringLiteral: Parser[StringLiteral] =
      ("\" *".r ~ """([^"\\]*(\\.)*)*""".r ~ " *\"".r)
        .map { case l ~ m ~ r => l.tail + m + r.init }
        .map(StringContext.processEscapes)
        .map(StringLiteral.apply)

    def longLiteral: Parser[LongLiteral] =
      """-?[0-9]+""".r ^^ { l => LongLiteral.apply(l.toLong) }

    def numberLiteral: Parser[NumberLiteral] =
      """-?[0-9]+\.[0-9]+""".r ^^ { n => NumberLiteral(BigDecimal.apply(n)) }

    def recordLiteral: Parser[RecordLiteral] =
      "{" ~ repsep(recordEntry, ",") ~ "}" ^^ { case _ ~ entries ~ _ =>
        RecordLiteral(entries.toMap)
      }

    def recordEntry: Parser[(String, PureExpression)] =
      recordFieldName ~ ":" ~ pureExpression ^^ { case field ~ _ ~ expr =>
        field -> expr
      }

    def recordFieldName: Parser[String] = name

    def variable: Parser[Variable] =
      name.map(Variable.apply)

    // TODO - repsep1 is not available
    def dotAccessor: Parser[DotAccessor] =
      dotAccessorSubject ~ "." ~ repsep(name, ".") ^^ { case subject ~ _ ~ chain =>
        DotAccessor(subject, chain)
      }

    def dotAccessorSubject: Parser[PureExpression] =
      variable
        | recordLiteral
        | "(" ~ literal ~ ")" ^^ { case _ ~ literal ~ _ => literal }

    def pureApplication: Parser[PureApplication] =
      name ~ "(" ~ rep1sep(pureExpression, ",") ~ ")" ^^ { case funcName ~ _ ~ args ~ _ =>
        PureApplication(funcName, args)
      }

    def ifExpression: Parser[PureExpression] =
      "if" ~ "(" ~ pureExpression ~ ")" ~ "{" ~
        extendedPureExpression ~
        "}" ~ "else" ~ "{" ~
        extendedPureExpression ~
        "}" ^^ { case _ ~ _ ~ condition ~ _ ~ _ ~ ifTrue ~ _ ~ _ ~ _ ~ ifFalse ~ _ =>
          IfExpression(condition, ifTrue, ifFalse)
        }

    // TODO - want this to produce a BinaryOp
    def binOp: Parser[PureExpression] =
      chainl1(pureTerm, op)

    // TODO - I've given no thought to precedence
    def op: Parser[(PureExpression, PureExpression) => BinaryOp] =
      ("==" | "+" | "-" | "*" | "/" | "%" | ">=" | ">" | "<=" | "<") ^^ {
        case "==" => { (l, r) => BinaryOp(EqOp, l, r) }
        case ">"  => { (l, r) => BinaryOp(GtOp, l, r) }
        case ">=" => { (l, r) => BinaryOp(GtEqOp, l, r) }
        case "<"  => { (l, r) => BinaryOp(LtOp, l, r) }
        case "<=" => { (l, r) => BinaryOp(LtEqOp, l, r) }
        case "+"  => { (l, r) => BinaryOp(AddOp, l, r) }
        case "-"  => { (l, r) => BinaryOp(SubtractOp, l, r) }
        case "*"  => { (l, r) => BinaryOp(MultOp, l, r) }
        case "/"  => { (l, r) => BinaryOp(DivOp, l, r) }
        case "%"  => { (l, r) => BinaryOp(ModOp, l, r) }
      }

    def returns: Parser[PureExpression] =
      "return" ~ pureExpression ^^ { case _ ~ expr =>
        expr
      }

  }

}
