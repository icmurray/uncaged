package icmurray.uncaged.parser

final case class ProgramDefn(
  main: IOFuncDefn,
  functions: List[FuncDefn]
)

sealed trait FuncDefn

final case class PureFuncDefn(
  name: String,
  args: List[Binder],
  statements: List[Statement],
  returns: PureExpression
) extends FuncDefn

final case class IOFuncDefn(
  name: String,
  args: List[Binder],
  statements: List[Statement],
  returns: PureExpression
) extends FuncDefn

final case class AnonBlock(
  statements: List[Statement],
  returns: PureExpression
)

sealed trait PureExpression
sealed trait Literal extends PureExpression

final case class StringLiteral(value: String) extends Literal
final case class LongLiteral(value: Long) extends Literal
final case class NumberLiteral(value: BigDecimal) extends Literal
final case class RecordLiteral(value: Map[String, PureExpression]) extends Literal

final case class Variable(name: String) extends PureExpression
final case class DotAccessor(subject: PureExpression, fields: List[String]) extends PureExpression

final case class BinaryOp(op: Op, left: PureExpression, right: PureExpression) extends PureExpression
final case class IfExpression(condition: PureExpression, ifTrue: PureExpression, ifFalse: PureExpression)
    extends PureExpression

final case class PureApplication(function: String, args: List[PureExpression]) extends PureExpression

/**
 * A function call application, but in a tail recursive position; meaning it can
 * be eliminated with a jump if the target language allows.
 **/
final case class JumpApplication(function: String, args: List[PureExpression]) extends PureExpression

sealed trait Op
case object AddOp extends Op
case object SubtractOp extends Op
case object MultOp extends Op
case object DivOp extends Op
case object ModOp extends Op
case object EqOp extends Op
case object GtOp extends Op
case object GtEqOp extends Op
case object LtOp extends Op
case object LtEqOp extends Op

sealed trait IOExpression

final case class Lift(pure: PureExpression) extends IOExpression

final case class HttpCall(
  method: String,
  url: PureExpression,
  query: PureExpression
) extends IOExpression

final case class Binder(name: String)

final case class Statement(
  binder: Binder,
  expression: IOExpression
)
