package icmurray.uncaged

import scala.io.Source

import pprint.pprintln

import icmurray.uncaged.ir
import icmurray.uncaged.backend.gcp.{Compiler => GCPCompiler, Generator}
import icmurray.uncaged.parser.Parser
import icmurray.uncaged.ir.Program
import icmurray.uncaged.parser.ProgramDefn

object Main {

  def main(args: Array[String]): Unit = {

    val source = Source.fromFile(args(0)).getLines.mkString
    val prog: Either[String, Program] =
      Parser()
        .parse(source)
        .map(compiler.Compiler.compile)
        .map(p => compiler.IdentifyTailCalls.run(p.definition))
        .map(ir.Compiler().compile)

    pprintln(prog)

    val gcpWf = prog.map(GCPCompiler().compile)

    println(Generator.generate(gcpWf.toOption.get))
  }

}
