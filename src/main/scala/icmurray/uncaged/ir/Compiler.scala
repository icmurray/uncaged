package icmurray.uncaged.ir

import alleycats.std.map._
import cats.data.{ReaderT, State}
import cats.implicits._

import icmurray.uncaged.{parser => p}
import icmurray.uncaged.compiler.GatherProgramInfo.ProgramInfo
import icmurray.uncaged.compiler.GatherProgramInfo
import icmurray.uncaged.compiler.GatherProgramInfo.FunctionInfo

/**
 * Transform the given program from its parsed representation to its reduced
 * intermediate representation.
 *
 * The input (parsed) program allows the following which must be removed from
 * the transformed result:
 *
 *  - conditionals (if-expressions) embedded within other expressions
 *  - function application enbedded within other expressions
 *
 * The strategy taken to remove both of these is to:
 *
 *  - lift the function application (or if-expression) up into a top-level of a
 *    Statement that binds the result to a newly generated variable.
 *  - rewrite the original expression to dereference the variable, instead of
 *    computing the function application (or if-expression).
 *  - this is done recursively through the structure
 *
 * For example, if the following input program is given (pseudo-code):
 *
 * {{{
 * def foo n =
 *   f(n) + f(g(n))
 * }}}
 *
 * Then this will be transformed into something like (pseudo-code):
 *
 * {{{
 * def foo n =
 *   let _b0 <- f(n)
 *   let _b1 <- g(n)
 *   let _b2 <- f(_b1)
 *   in _b0 + _b2
 * }}}
 *
 * Because the function applications are now at the top-level of a Statement,
 * which the intermediate language allows (and requires).
 **/
trait Compiler {
  def compile(prog: p.ProgramDefn): Program
}

object Compiler {

  private type CompilerState[A] = State[NameSource, A]
  private type Env[A] = ReaderT[CompilerState, ProgramInfo, A]

  private object Env {
    def pure[A](a: A): Env[A] = ReaderT.pure(a)
    def lift(e: SimpleExpression): Env[Reduced] = Env.pure(Reduced(Nil, e))
    val nextBinder: Env[Binder] = ReaderT.liftF(NameSource.nextBinder)
    def functionInfo(f: String): Env[FunctionInfo] =
      for {
        info <- ReaderT.ask
        fInfo = info.functions.find(_.name == f)
      } yield fInfo.get // TODO - model possible failure
  }

  private case class NameSource(binder: Int) {
    def nextBinder = (this.copy(binder = binder + 1), Binder(s"_b_$binder"))
  }

  private object NameSource {
    def init: NameSource = NameSource(0)
    val nextBinder: State[NameSource, Binder] = State(_.nextBinder)
  }

  private case class Reduced(statements: List[Statement], expression: SimpleExpression)

  def apply(): Compiler = new Impl()

  private class Impl extends Compiler {

    def compile(prog: p.ProgramDefn): Program = {

      val info = GatherProgramInfo.apply(prog)

      val process = for {
        main <- compile(prog.main)
        functions <- prog.functions.map(compile).sequence
      } yield Program(main, functions)

      process.run(info).runA(NameSource.init).value
    }

    private def compile(f: p.FuncDefn): Env[FuncDefn] = f match {
      case f: p.PureFuncDefn => compilePure(f)
      case f: p.IOFuncDefn   => compileIO(f)
    }

    private def compilePure(f: p.PureFuncDefn): Env[FuncDefn] =
      for {
        statements <- reduce(f.statements)
        reduced <- reduce(f.returns)
        irF = FuncDefn(
          name = f.name,
          args = f.args.map(b => Ident(b.name)),
          block = Block(statements ++ reduced.statements, reduced.expression)
        )
      } yield irF

    private def compileIO(f: p.IOFuncDefn): Env[FuncDefn] =
      for {
        statements <- reduce(f.statements)
        reduced <- reduce(f.returns)
        irF = FuncDefn(
          name = f.name,
          args = f.args.map(b => Ident(b.name)),
          block = Block(statements ++ reduced.statements, reduced.expression)
        )
      } yield irF

    private def reduce(ss: List[p.Statement]): Env[List[Statement]] =
      ss.map(reduce).sequence.map(_.flatten)

    private def reduce(s: p.Statement): Env[List[Statement]] = s.expression match {
      case p.Lift(expr) =>
        for {
          reduced <- reduce(expr)
          assign = Statement(Binder(s.binder.name), Lift(reduced.expression))
        } yield reduced.statements :+ assign

      case p.HttpCall(method, url, query) =>
        for {
          reducedUrl <- reduce(url)
          reducedQuery <- reduce(query)
          http = HttpCall(method, reducedUrl.expression, reducedQuery.expression)
          assign = Statement(Binder(s.binder.name), http)
        } yield reducedUrl.statements ++ reducedQuery.statements :+ assign
    }

    private def reduce(e: p.PureExpression): Env[Reduced] = e match {
      case p.StringLiteral(s)  => Env.lift(StringLiteral(s))
      case p.LongLiteral(l)    => Env.lift(LongLiteral(l))
      case p.NumberLiteral(bd) => Env.lift(NumberLiteral(bd))
      case p.Variable(name)    => Env.lift(Variable(Ident(name)))

      case p.RecordLiteral(r) =>
        for {
          reduced <- r.toList.map { (k, v) => reduce(v).map(k -> _) }.sequence
          statements = reduced.map(_._2).flatMap(_.statements)
          record = RecordLiteral(reduced.map((k, r) => k -> r.expression).toMap)
        } yield Reduced(statements, record)

      case p.DotAccessor(subject, chain) =>
        for {
          reduced <- reduce(subject)
          accessor = DotAccessor(reduced.expression, chain)
        } yield Reduced(reduced.statements, accessor)

      case p.BinaryOp(op, left, right) =>
        for {
          reduceL <- reduce(left)
          reduceR <- reduce(right)
          binOp = BinaryOp(op, reduceL.expression, reduceR.expression)
        } yield Reduced(reduceL.statements ++ reduceR.statements, binOp)

      case p.IfExpression(cond, ifT, ifF) =>
        for {
          reducedCond <- reduce(cond)
          reducedT <- reduce(ifT)
          reducedF <- reduce(ifF)
          ifE = IfExpression(
            condition = reducedCond.expression,
            ifTrue = Block(reducedT.statements, reducedT.expression),
            ifFalse = Block(reducedF.statements, reducedF.expression)
          )
          binder <- Env.nextBinder
          assign = Statement(binder, ifE)
        } yield Reduced(reducedCond.statements :+ assign, Variable(Ident(binder.name)))

      case p.PureApplication(f, args) =>
        for {
          reducedArgs <- args.map(reduce).sequence
          app = Application(function = Ident(f), args = reducedArgs.map(_.expression))
          binder <- Env.nextBinder
          assign = Statement(binder, app)
        } yield Reduced(reducedArgs.flatMap(_.statements) :+ assign, Variable(Ident(binder.name)))

      // tail-call elimination
      case p.JumpApplication(f, args) =>
        for {
          reducedArgs <- args.map(reduce).sequence
          info <- Env.functionInfo(f)
          tmpAssign <- reducedArgs.map(_.expression).zip(info.params).map { (arg, param) =>
            Env.nextBinder.map { b => Statement(b, Lift(arg)) }
          }.sequence
          reAssign = tmpAssign.zip(info.params).map { (s, param) =>
            Statement(Binder(param), Lift(Variable(Ident(s.binder.name))))
          }
        } yield Reduced(reducedArgs.flatMap(_.statements) ++ tmpAssign ++ reAssign, Jump(Ident(f)))
    }

  }

}
