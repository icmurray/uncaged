package icmurray.uncaged.ir

import icmurray.uncaged.parser

final case class Program(
  main: FuncDefn,
  functions: List[FuncDefn]
)

final case class FuncDefn(
  name: String,
  args: List[Ident],
  block: Block
)

final case class Block(
  statements: List[Statement],
  returns: SimpleExpression
)

sealed trait SimpleExpression

sealed trait Literal extends SimpleExpression

final case class StringLiteral(value: String) extends Literal
final case class LongLiteral(value: Long) extends Literal
final case class NumberLiteral(value: BigDecimal) extends Literal
final case class RecordLiteral(value: Map[String, SimpleExpression]) extends Literal

final case class Variable(ident: Ident) extends SimpleExpression
final case class DotAccessor(subject: SimpleExpression, fields: List[String]) extends SimpleExpression
final case class BinaryOp(op: parser.Op, left: SimpleExpression, right: SimpleExpression) extends SimpleExpression

// TODO - I'd like to try to eliminate this
final case class Jump(function: Ident) extends SimpleExpression

final case class Ident(name: String)
final case class Binder(name: String)

sealed trait CompositeExpression
final case class Lift(expr: SimpleExpression) extends CompositeExpression
final case class Application(function: Ident, args: List[SimpleExpression]) extends CompositeExpression
final case class IfExpression(condition: SimpleExpression, ifTrue: Block, ifFalse: Block) extends CompositeExpression
final case class HttpCall(method: String, url: SimpleExpression, query: SimpleExpression) extends CompositeExpression

sealed trait Instruction
final case class Statement(binder: Binder, expression: CompositeExpression) extends Instruction
