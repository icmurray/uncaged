package icmurray.uncaged.backend.gcp

sealed trait Expression
sealed trait Assignable extends Expression
sealed trait Value extends Expression

final case class Variable(name: String) extends Assignable

final case class StringValue(s: String) extends Value
final case class IntegerValue(l: Long) extends Value
final case class DoubleValue(d: Double) extends Value
final case class MapValue(elements: Map[String, Expression]) extends Value

final case class DotAccessor(subject: Expression, fields: List[String]) extends Expression

final case class BinaryOp(op: BinaryOp.Op, left: Expression, right: Expression) extends Expression

object BinaryOp {

  sealed trait Op

  case object `*` extends Op
  case object `/` extends Op
  case object `//` extends Op
  case object `%` extends Op

  case object `+` extends Op
  case object `-` extends Op

  case object `<` extends Op
  case object `<=` extends Op
  case object `>=` extends Op
  case object `>` extends Op
  case object Eq extends Op
  case object Neq extends Op
  case object In extends Op

  case object And extends Op
  case object Or extends Op

}
