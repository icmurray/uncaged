package icmurray.uncaged.backend.gcp

import icmurray.uncaged.ir
import cats.data.{ReaderT, State}
import cats.implicits._
import icmurray.uncaged.compiler.GatherProgramInfo.{FunctionInfo, ProgramInfo}
import icmurray.uncaged.compiler.GatherProgramInfo
import icmurray.uncaged.{parser => p}

trait Compiler {
  def compile(program: ir.Program): Workflow
}

object Compiler {

  def apply(): Compiler = new Impl

  /**
   * Has capability to mark a function entry so that the first step for the latest
   * marked function can be retrieved.  This is used for jumps.
   **/
  final private case class NameSource(step: Int, functionMark: Option[Int]) {
    def nextStep = (this.copy(step = step + 1), Step.Name(s"_step_$step"))
    def markFunction = (this.copy(functionMark = Some(step)), ())
    def currentFunction = (this, Step.Name(s"_step_${functionMark.get}")) // TODO - model failure
  }

  private object NameSource {
    val init = NameSource(0, None)
    val nextStep: State[NameSource, Step.Name] = State(_.nextStep)
    val markFunction: State[NameSource, Unit] = State(_.markFunction)
    val currentFunction: State[NameSource, Step.Name] = State(_.currentFunction)
  }

  private type CompilerState[A] = State[NameSource, A]
  private type Env[A] = ReaderT[CompilerState, ProgramInfo, A]

  private object Env {

    val nextStep: Env[Step.Name] = ReaderT.liftF(NameSource.nextStep)
    val markFunction: Env[Unit] = ReaderT.liftF(NameSource.markFunction)
    val currentFunction: Env[Step.Name] = ReaderT.liftF(NameSource.currentFunction)

    def functionInfo(name: ir.Ident): Env[FunctionInfo] =
      for {
        info <- ReaderT.ask
        f = info.functions.find(_.name == name.name)
      } yield f.get // TODO - model possible failure
  }

  private class Impl extends Compiler {

    override def compile(program: ir.Program): Workflow = {

      val info = GatherProgramInfo(program)

      val compilation = for {
        main <- compile(program.main)
        functions <- program.functions.map(compile).sequence
        workflow = Workflow(main, functions)
      } yield workflow

      val nonOptimal = compilation.run(info).runA(NameSource.init).value
      StepMerge.apply(nonOptimal)
    }

    private def compile(f: ir.FuncDefn): Env[Block] =
      for {
        _ <- Env.markFunction
        statements <- f.block.statements.map(compile).sequence
        returns <- returnStep(f.block.returns)
        block = Block(
          name = Block.Name(f.name),
          params = f.args.map(a => Block.NamedParameter(a.name)),
          steps = statements :+ returns
        )
      } yield block

    private def compile(s: ir.Statement): Env[Step] = s match {
      case ir.Statement(binder, ir.Lift(ir.Jump(f)))     => jumpStep(f.name).widen[Step]
      case ir.Statement(binder, ir.Lift(e))              => assignStep(binder, e).widen[Step]
      case ir.Statement(binder, app: ir.Application)     => callStep(binder, app).widen[Step]
      case ir.Statement(binder, ifExpr: ir.IfExpression) => ifStep(binder, ifExpr).widen[Step]
      case ir.Statement(binder, ir.HttpCall(_, _, _))    => ???
    }

    private def jumpStep(f: String): Env[AssignStep] =
      for {
        functionStep <- Env.currentFunction
        name <- Env.nextStep
        step = AssignStep(
          name = name,
          assignments = Nil,
          next = Some(functionStep)
        )
      } yield step

    private def assignStep(b: ir.Binder, expr: ir.SimpleExpression): Env[AssignStep] =
      for {
        name <- Env.nextStep
        step = AssignStep(
          name = name,
          assignments = Step.Assignment(Variable(b.name), gcpExpression(expr)) :: Nil,
          next = None
        )
      } yield step

    private def callStep(b: ir.Binder, app: ir.Application): Env[CallStep] =
      for {
        name <- Env.nextStep
        info <- Env.functionInfo(app.function)
        args = info.params.zip(app.args).toMap.view.mapValues(gcpExpression).toMap
        step = CallStep(
          name = name,
          call = BlockingCall(app.function.name),
          args = args,
          result = Some(Variable(b.name)),
          next = None
        )
      } yield step

    private def ifStep(b: ir.Binder, ifExpr: ir.IfExpression): Env[SwitchStep] =
      for {
        name <- Env.nextStep
        condition = gcpExpression(ifExpr.condition)

        ifTrueSteps <- ifExpr.ifTrue.statements.map(compile).sequence

        // TODO - I'd like the IR to allow this to be avoided
        ifTrueReturns <- ifExpr.ifTrue.returns match {
          case ir.Jump(f) => jumpStep(f.name)
          case other => assignStep(b, other)
        }

        ifFalseSteps <- ifExpr.ifFalse.statements.map(compile).sequence
        ifFalseReturns <- ifExpr.ifFalse.returns match {
          case ir.Jump(f) => jumpStep(f.name)
          case other => assignStep(b, other)
        }

        step = SwitchStep(
          name = name,
          branches = Step.Branch(condition = condition, ifTrueSteps :+ ifTrueReturns) :: Nil,
          default = Some(ifFalseSteps :+ ifFalseReturns),
          next = None
        )
      } yield step

    private def returnStep(e: ir.SimpleExpression): Env[ReturnStep] =
      for {
        name <- Env.nextStep
        step = ReturnStep(name = name, returns = gcpExpression(e))
      } yield step

    private def gcpExpression(e: ir.SimpleExpression): Expression =
      e match {
        case ir.StringLiteral(s)             => StringValue(s)
        case ir.LongLiteral(l)               => IntegerValue(l)
        case ir.NumberLiteral(bd)            => DoubleValue(bd.toDouble)
        case ir.RecordLiteral(elements)      => MapValue(elements.view.mapValues(gcpExpression).toMap)
        case ir.Variable(ir.Ident(name))     => Variable(name)
        case ir.DotAccessor(subject, fields) => DotAccessor(gcpExpression(subject), fields)
        case ir.BinaryOp(op, left, right)    => BinaryOp(gcpOp(op), gcpExpression(left), gcpExpression(right))
        case ir.Jump(f) => throw new Exception(s"Unhandled jump to $f") // TODO improve model so this can't exist
      }

    private def gcpOp(op: p.Op): BinaryOp.Op = op match {
      case p.AddOp      => BinaryOp.`+`
      case p.SubtractOp => BinaryOp.`-`
      case p.MultOp     => BinaryOp.`*`
      case p.DivOp      => BinaryOp.`/`
      case p.ModOp      => BinaryOp.`%`
      case p.EqOp       => BinaryOp.Eq
      case p.GtOp       => BinaryOp.`>`
      case p.GtEqOp     => BinaryOp.`>=`
      case p.LtOp       => BinaryOp.`<`
      case p.LtEqOp     => BinaryOp.`<=`
    }
  }

}
