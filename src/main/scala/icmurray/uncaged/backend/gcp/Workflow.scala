package icmurray.uncaged.backend.gcp

final case class Workflow(
  main: Block,
  subBlocks: Seq[Block]
)
