package icmurray.uncaged.backend.gcp

import icmurray.uncaged.backend.gcp.Block._

final case class Block(
  name: Name,
  params: Seq[Parameter],
  steps: Seq[Step]
)

object Block {

  final case class Name(value: String)

  sealed trait Parameter
  final case class NamedParameter(name: String) extends Parameter
  // final case class DefaultParameter(name: String, defaultValue: Value) extends Parameter

}
