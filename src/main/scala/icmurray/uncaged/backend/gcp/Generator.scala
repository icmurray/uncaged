package icmurray.uncaged.backend.gcp

import io.circe._
import io.circe.syntax._

import icmurray.uncaged.backend.gcp.Step.{Assignment, Branch}

object Generator {

  def generate(program: Workflow): Json = program.asJson

  implicit private val workflow: Encoder[Workflow] =
    new Encoder[Workflow] {
      override def apply(w: Workflow): Json = {
        val main = w.main.asJson
        val subBlocks = w.subBlocks.map { b => b.name.value -> b.asJson }
        val blocks = ("main" -> main) +: subBlocks
        Json.obj(blocks: _*)
      }
    }

  implicit private val block: Encoder[Block] =
    new Encoder[Block] {
      override def apply(b: Block): Json = {

        val params = b.params.map(_.asJson).asJson
        val steps = b.steps.map { s => Json.obj(s.name.value -> s.asJson) }.asJson

        Json.obj(
          "params" -> params,
          "steps" -> steps
        )
      }
    }

  implicit private val param: Encoder[Block.Parameter] =
    new Encoder[Block.Parameter] {
      override def apply(p: Block.Parameter): Json =
        p match {
          case Block.NamedParameter(name) => name.asJson
        }
    }

  implicit private val step: Encoder[Step] =
    new Encoder[Step] {
      override def apply(s: Step): Json =
        s match {
          case s: AssignStep => assignStep.apply(s)
          case s: CallStep   => callStep.apply(s)
          case s: ReturnStep => returnStep.apply(s)
          case s: SwitchStep => switchStep.apply(s)
        }
    }

  implicit private val assignStep: Encoder[AssignStep] =
    new Encoder[AssignStep] {
      override def apply(s: AssignStep): Json = {
        val assignments = s.assignments.map(_.asJson).asJson
        val base = Map.apply[String, Json]("assign" -> assignments)
        s.next match {
          case None => base.asJson
          case Some(n) => (base + ("next" -> n.value.asJson)).asJson
        }
      }
    }

  implicit private val callStep: Encoder[CallStep] =
    new Encoder[CallStep] {
      override def apply(s: CallStep): Json = {
        val base = Map.apply[String, Json](
          "call" -> s.call.target.asJson,
          "args" -> s.args.asJson
        )

        s.result.fold(base)(r => base + ("result" -> r.asJson(implicitly[Encoder[Assignable]]))).asJson
      }
    }

  implicit private val returnStep: Encoder[ReturnStep] =
    new Encoder[ReturnStep] {
      override def apply(r: ReturnStep): Json =
        Json.obj(
          "return" -> r.returns.asJson
        )
    }

  implicit private val switchStep: Encoder[SwitchStep] =
    new Encoder[SwitchStep] {
      override def apply(r: SwitchStep): Json = {

        val branches = r.branches ++ r.defaultBranch

        Json.obj(
          "switch" -> branches.asJson
        )
      }
    }

  implicit private val branch: Encoder[Branch] =
    new Encoder[Branch] {
      override def apply(b: Branch): Json =
        Json.obj(
          "condition" -> b.condition.asJson,
          "steps" -> b.body.map { s => Json.obj(s.name.value -> s.asJson) }.asJson
        )
    }

  implicit private val assignment: Encoder[Assignment] =
    new Encoder[Assignment] {
      override def apply(a: Assignment): Json =
        Json.obj(
          KeyEncoder[Assignable].apply(a.variable) -> a.expression.asJson
        )
    }

  implicit private val expression: Encoder[Expression] =
    new Encoder[Expression] {
      override def apply(e: Expression): Json =
        e match {
          case v: Variable        => wrap(unwrapped(v)).asJson
          case StringValue(s)     => s.asJson
          case IntegerValue(l)    => l.asJson
          case DoubleValue(d)     => d.asJson
          case MapValue(elements) => elements.asJson
          case d: DotAccessor     => wrap(unwrapped(d)).asJson
          case b: BinaryOp        => wrap(unwrapped(b)).asJson
        }

      private def unwrapped(e: Expression): String =
        e match {
          case Variable(name)               => name
          case StringValue(s)               => "\"" + s + "\""
          case IntegerValue(l)              => l.toString
          case DoubleValue(d)               => d.toString
          case MapValue(elements)           => throw new Exception("TODO Unprocessable MapValue as string")
          case DotAccessor(subject, fields) => "(" + unwrapped(subject) + ")." + fields.mkString(".")
          case BinaryOp(op, left, right)    => "(" + unwrapped(left) + ") " + opRepr(op) + " (" + unwrapped(right) + ")"
        }

      private def wrap(s: String) = "${" + s + "}"
    }

  private def opRepr(op: BinaryOp.Op): String = op match {
    case BinaryOp.`*`  => "*"
    case BinaryOp.`/`  => "/"
    case BinaryOp.`//` => "//"
    case BinaryOp.`%`  => "%"

    case BinaryOp.`+` => "+"
    case BinaryOp.`-` => "-"

    case BinaryOp.`<`  => "<"
    case BinaryOp.`<=` => "<="
    case BinaryOp.`>=` => ">="
    case BinaryOp.`>`  => ">"
    case BinaryOp.Eq   => "=="
    case BinaryOp.Neq  => "!="
    case BinaryOp.In   => "in"

    case BinaryOp.And => "&&"
    case BinaryOp.Or  => "||"
  }

  implicit private val assignable: Encoder[Assignable] =
    new Encoder[Assignable] {
      override def apply(key: Assignable): Json =
        key match {
          case Variable(name) => name.asJson
        }
    }

  implicit private val assignableKey: KeyEncoder[Assignable] =
    new KeyEncoder[Assignable] {
      override def apply(key: Assignable): String =
        key match {
          case Variable(name) => name
        }
    }
}
