package icmurray.uncaged.backend.gcp

trait StepMerge {
  def apply(wf: Workflow): Workflow
}

object StepMerge {

  def apply: StepMerge = new Impl()

  private class Impl extends StepMerge {

    override def apply(wf: Workflow) =
      Workflow(
        main = merge(wf.main),
        subBlocks = wf.subBlocks.map(merge)
      )

    private def merge(b: Block): Block =
      b.copy(steps = merge(b.steps))

    private def merge(s: Seq[Step]): Seq[Step] = {
      def go(s: Seq[Step]): Seq[Step] =
        s.foldLeft(List.empty[Step]) {
          case (ss, sw: SwitchStep) =>
            val newSwitch = sw.copy(
              branches = sw.branches.map(merge),
              default = sw.default.map(merge)
            )
            newSwitch :: ss
          case ((a1: AssignStep) :: rest, a2: AssignStep) if canMerge(a1, a2) => merge(a1, a2) :: rest
          case (ss, s) => s :: ss
        }

      go(s).reverse
    }

    private def merge(b: Step.Branch): Step.Branch =
      b.copy(body = merge(b.body))

    /**
     * a1 ; a2
     * */
    private def canMerge(a1: AssignStep, a2: AssignStep): Boolean =
      a1.next.isEmpty

    /**
     * a1 ; a2
     * */
    private def merge(a1: AssignStep, a2: AssignStep): AssignStep =
      AssignStep(
        name = a1.name, // retain first step so that it can be jumped to (not 100% correct)
        assignments = a1.assignments ++ a2.assignments,
        next = a2.next
      )
  }

}
