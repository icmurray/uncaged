package icmurray.uncaged.backend.gcp

import icmurray.uncaged.backend.gcp.Step._

sealed trait Step {
  val name: Name
  val next: Option[Addr]
}

final case class AssignStep(
  name: Name,
  assignments: Seq[Assignment],
  next: Option[Name]
) extends Step

final case class CallStep(
  name: Name,
  call: BlockingCall,
  args: Map[String, Expression],
  result: Option[Variable],
  next: Option[Name]
) extends Step

final case class ReturnStep(
  name: Name,
  returns: Expression
) extends Step {
  override val next = Some(Return(returns))
}

final case class SwitchStep(
  name: Name,
  branches: Seq[Branch],
  default: Option[Seq[Step]],
  next: Option[Name]
) extends Step {
  val defaultBranch: Option[Branch] = default.map { steps =>
    Branch(BinaryOp(BinaryOp.Eq, IntegerValue(0), IntegerValue(0)), steps)
  }
}

object Step {

  sealed trait Addr
  final case class Name(value: String) extends Addr
  final case class Return(expr: Expression) extends Addr
  object End extends Addr

  final case class Assignment(
    variable: Assignable,
    expression: Expression
  )

  final case class Branch(
    condition: Expression,
    body: Seq[Step]
  )

}
