package icmurray.uncaged.compiler

import icmurray.uncaged.{parser => p}
import icmurray.uncaged.parser.IOFuncDefn

trait IdentifyTailCalls {
  def run(prog: p.ProgramDefn): p.ProgramDefn
}

object IdentifyTailCalls extends IdentifyTailCalls {

  override def run(prog: p.ProgramDefn): p.ProgramDefn =
    p.ProgramDefn(
      main = runIO(prog.main),
      functions = prog.functions.map(run)
    )

  private def run(f: p.FuncDefn): p.FuncDefn = f match {
    case f: p.IOFuncDefn => runIO(f)
    case f: p.PureFuncDefn => runPure(f)
  }

  private def runIO(f: p.IOFuncDefn): p.IOFuncDefn =
    f.copy(returns = run(f.name, f.returns))

  private def runPure(f: p.PureFuncDefn): p.PureFuncDefn =
    f.copy(returns = run(f.name, f.returns))

  private def run(containingFunction: String, e: p.PureExpression): p.PureExpression = e match {
    case p.PureApplication(f, args) if f == containingFunction => p.JumpApplication(f, args)
    case p.IfExpression(cond, ifT, ifF) => p.IfExpression(cond, run(containingFunction, ifT), run(containingFunction, ifF))
    case other => other
  }

}
