package icmurray.uncaged.compiler

import icmurray.uncaged.ir
import icmurray.uncaged.parser.{FuncDefn, IOFuncDefn, ProgramDefn, PureFuncDefn}

object GatherProgramInfo {

  final case class ProgramInfo(functions: Seq[FunctionInfo])

  final case class FunctionInfo(name: String, params: Seq[String])

  def apply(program: ProgramDefn): ProgramInfo =
    ProgramInfo(
      (program.main +: program.functions).foldLeft(Seq.empty) { (fs, f) =>
        mkFunctionInfo(f) +: fs
      }
    )

  def apply(program: ir.Program): ProgramInfo =
    ProgramInfo(
      (program.main +: program.functions).foldLeft(Seq.empty) { (fs, f) =>
      mkFunctionInfo(f) +: fs
    }
    )

  private def mkFunctionInfo(f: FuncDefn): FunctionInfo =
    f match {
      case f: PureFuncDefn => FunctionInfo(f.name, f.args.map(_.name))
      case f: IOFuncDefn   => FunctionInfo(f.name, f.args.map(_.name))
    }

  private def mkFunctionInfo(f: ir.FuncDefn): FunctionInfo =
    FunctionInfo(f.name, f.args.map(_.name))
}
