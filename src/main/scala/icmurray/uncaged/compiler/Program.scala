package icmurray.uncaged.compiler

import icmurray.uncaged.compiler.GatherProgramInfo.ProgramInfo
import icmurray.uncaged.parser.ProgramDefn

final case class Program(definition: ProgramDefn, info: ProgramInfo)
