package icmurray.uncaged.compiler

import icmurray.uncaged.compiler.GatherProgramInfo.ProgramInfo
import icmurray.uncaged.parser.ProgramDefn

object Compiler {

  def compile(program: ProgramDefn): Program =
    Program(program, GatherProgramInfo.apply(program))

}
