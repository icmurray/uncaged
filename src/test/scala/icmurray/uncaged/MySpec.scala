package icmurray.uncaged

import org.scalatest._
import org.scalatest.matchers._
import org.scalatest.wordspec.AnyWordSpec

class MySpec extends AnyWordSpec with should.Matchers {

  "example test" should {
    "succeed" in {
      val obtained = 42
      val expected = 42
      obtained shouldEqual expected
    }
  }

}
