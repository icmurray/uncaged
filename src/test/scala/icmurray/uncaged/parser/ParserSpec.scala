package icmurray.uncaged.parser

import org.scalatest._
import org.scalatest.matchers._
import org.scalatest.wordspec.AnyWordSpec

class ParserSpec extends AnyWordSpec with should.Matchers {

  "parser" should {

    "succeed with basic assignment" in {

      val program = """
        | def main args = do
        |   let fullname = args.first_name + " " + args.last_name
        |   let height_cms = imperialToMetric(args.height)
        |   return { name: fullname, height: height_cms };
        |
        | def imperialToMetric lengthInches =
        |   let cmsInInch = 2.54
        |   in lengthInches * cmsInInch;
      """.stripMargin

      val result = Parser().parse(program)
      assert(result.isRight)
    }

    "succeed with dependent http calls" in {

      val program = """
        | def main args = do
        |   let datetime_url = "https://us-central1-workflowsample.cloudfunctions.net/datetime"
        |   time_response <- http.get(datetime_url)
        |   let wiki_url = "https://en.wikipedia.org/w/api.php"
        |   let wiki_params = {
        |     action: "opersearch",
        |     search: time_response.body.dayOfTheWeek
        |   }
        |   wiki_response <- http.get(wiki_url, wiki_params)
        |   return wiki_response;
      """.stripMargin

      val result = Parser().parse(program)
      assert(result.isRight)
    }

    "write fact" in {

      val program = """
        | def main args = do
        |   let result = fact(args.n)
        |   return { result: result };
        |
        | def fact n =
        |   let res = if (n == 1) {
        |     1
        |   } else {
        |     n * fact (n-1)
        |   }
        |   in res;
      """.stripMargin

      val result = Parser().parse(program)
      assert(result.isRight)
    }

    "parse multiple functions" in {

      val program = """
        | def main args = do
        |   let result = f1(args.n)
        |   return { result: result };
        |
        | def f1 n =
        |   let r = f2(n) + f2(n)
        |   in r;
        |
        | def f2 n =
        |   let r = 2 * n
        |   in r;
      """.stripMargin

      val result = Parser().parse(program)
      assert(result.isRight)
    }

    "parse a pure function with only a pure expression value" in {

      val program = """
        | def main args = do
        |   let result = f1(args.n)
        |   return { result: result };
        |
        | def f1 n =
        |   f2(n, n) + 9;
        |
        | def f2 a b =
        |   a + 2 * b;
      """.stripMargin

      val result = Parser().parse(program)
      assert(result.isRight)
    }

    "parse a pure function with an if-expression result value" in {

      val program = """
        | def main args = do
        |   let result = fact(args.n)
        |   return { result: result };
        |
        | def fact n =
        |   if (n <= 1) { 1 } else { n * fact(n-1) };
      """.stripMargin

      val result = Parser().parse(program)
      assert(result.isRight)
    }
  }

}
