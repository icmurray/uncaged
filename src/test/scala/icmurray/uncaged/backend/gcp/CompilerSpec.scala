package icmurray.uncaged.backend.gcp

import org.scalatest._
import org.scalatest.matchers._
import org.scalatest.wordspec.AnyWordSpec

import icmurray.uncaged.backend.gcp
import icmurray.uncaged.compiler
import icmurray.uncaged.parser.Parser
import icmurray.uncaged.ir

class CompilerSpec extends AnyWordSpec with should.Matchers {

  "gcp compiler" should {

    "succeed with basic assignment" in {

      val program = """
        | def main args = do
        |   let fullname = args.first_name + " " + args.last_name
        |   let height_cms = imperialToMetric(args.height)
        |   return { name: fullname, height: height_cms };
        |
        | def imperialToMetric lengthInches =
        |   let cmsInInch = 2.54
        |   in lengthInches * cmsInInch;
      """.stripMargin

      val result =
        Parser().parse(program)
          .map(compiler.Compiler.compile)
          .map(p => ir.Compiler().compile(p.definition))
          .map(gcp.Compiler().compile)

      assert(result.isRight)
    }

    "succeed with fact" in {

      val program = """
        | def main args = do
        |   let result = fact(args.n)
        |   return { result: result };
        |
        | def fact n =
        |   let res = if (n == 1) {
        |     1
        |   } else {
        |     n * fact (n-1)
        |   }
        |   in res;
      """.stripMargin

      val result =
        Parser().parse(program)
          .map(compiler.Compiler.compile)
          .map(p => ir.Compiler().compile(p.definition))
          .map(gcp.Compiler().compile)

      assert(result.isRight)
    }

    "succeed with embedded if statements" in {

      val program = """
        | def main args = do
        |   let result = fib(args.n)
        |   return { fib: result };
        |
        | def fib n =
        |   let res = if (n <= 1) {
        |     if (n == 0) {
        |       0
        |     } else {
        |       1
        |     }
        |   } else {
        |     fib(n-2) + fib(n-1)
        |   }
        |   in res;
      """.stripMargin

      val result =
        Parser().parse(program)
          .map(compiler.Compiler.compile)
          .map(p => ir.Compiler().compile(p.definition))
          .map(gcp.Compiler().compile)

      assert(result.isRight)
    }

    "succeed with if-expression elimination in return position" in {

      val program = """
        | def main args = do
        |   let result = fact(args.n)
        |   return { result: result };
        |
        | def fact n =
        |   if (n <= 1) { 1 } else { n * fact(n-1) };
      """.stripMargin

      val result = Parser().parse(program)
          .map(compiler.Compiler.compile)
          .map(p => ir.Compiler().compile(p.definition))
          .map(gcp.Compiler().compile)

      assert(result.isRight)
    }

  }

}
